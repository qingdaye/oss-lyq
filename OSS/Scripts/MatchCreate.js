﻿$(function () {
    updatePlayer();
    updateMatchs();
    updateMatchPKInfo();
});
//添加比赛
$("#playersAdd").click(function () {
    var matchChoose = $("#matchChoose").val();
    var player1Add = $("#player1Add").val();
    var player2Add = $("#player2Add").val();
    var player1Song = $("#player1Song").val();
    var player2Song = $("#player2Song").val();

    if (matchChoose == 0 || player1Add == 0 || player2Add == 0) {
        alert("请选择场次或选手");
        return false;
    } else if (player1Song == "" || player2Song == "") {
        alert("请填写选手表演节目名称");
        return false;
    }
    var data = { matchChoose: matchChoose, player1Add: player1Add, player2Add: player2Add, player1Song: player1Song, player2Song: player2Song };
    $.ajax({
        url: "/Match/Create",
        data: data,
        type: "POST",
        dataType: "JSON",
        success: function () {
            updateMatchPKInfo();
        },
        error: function (date) {
            window.log("CreateMatchPKInfoError" + date);
        }
    });

});

//获取players
function updatePlayer() {
    //获取选手信息
    $.ajax({
        url: "/Match/GetMatchInfo?Type=1",
        type: "POST",
        async: false,
        dataType: "json",
        success: function (date) {
            var playerHtml = "";
            for (var item in date) {
                playerHtml += "<option value='" + date[item].ID + "'>" + date[item].PlayerName + "</option>";
            }
            $("#player1Add").append(playerHtml);
            $("#player2Add").append(playerHtml);
        },
        error: function (date) {
            alert("getPlayersError" + date);
        }
    });
}

//获取Matchs
function updateMatchs() {
    //获取Match信息
    $.ajax({
        url: "/Match/GetMatchInfo?Type=2",
        type: "POST",
        async: false,
        dataType: "json",
        success: function (date) {
            var matchHtml = "";
            for (var item in date) {
                matchHtml += "<option value='" + date[item].ID + "'>" + date[item].MatchName + "</option>";
            }
            $("#matchChoose").append(matchHtml);
        },
        error: function (date) { }
    });
}

//获取MatchPKInfo
function updateMatchPKInfo() {

    $("table .drop").remove();
    //获取比赛详细信息
    $.ajax({
        url: "/Match/GetMatchInfo?Type=3",
        type: "POST",
        async: false,
        dataType: "json",
        success: function (date) {
            var htmlText = "";
            for (var item in date) {
                var State;
                var BtnText;
                var PuaseState;
                var ChangeState;
                switch (date[item].BattleFlag) {
                    case 0: State = "未开始"; BtnText = "开始"; ChangeState = ""; PuaseState = "disabled"; break;
                    case 1: State = "正在投票"; BtnText = "停止"; ChangeState = ""; PuaseState = ""; break;
                    case 2: State = "暂停投票"; BtnText = "开始"; ChangeState = ""; PuaseState = "disabled"; break;
                    case 3: State = "结束投票"; BtnText = "开始"; ChangeState = "disabled"; PuaseState = "disabled"; break;
                    default: State = "未知状态"; BtnText = "结束"; ChangeState = "disabled"; PuaseState = "disabled";
                }
                htmlText +=
                    "<tr class='drop'>" +
                    "<td>" + date[item].Matchs.MatchName + "</td>" +
                    //第一名选手
                    "<td>" + date[item].FirstPlayer.PlayerName + "</td>" +
                    "<td>" + date[item].FirstSongName + "</td>" +
                    "<td>" + date[item].FirstTickCount + "</td>" +
                    "<td>" + date[item].FirstNeedScore + "</td>" +
                    "<td>" + date[item].FirstScore + "</td>" +
                    //第二名选手
                    "<td>" + date[item].SecondPlayer.PlayerName + "</td>" +
                    "<td>" + date[item].SecondSongName + "</td>" +
                    "<td>" + date[item].SecondTickCount + "</td>" +
                    "<td>" + date[item].SecondNeedScore + "</td>" +
                    "<td>" + date[item].SecondScore + "</td>" +
                    //状态与操作
                    "<td>" + State + "</td>" +
                    "<td><input type='button' onclick='stateChange(" + date[item].ID + ")'" + ChangeState + "  value='" + BtnText + "' />" +
                    "<input type= 'button' onclick='statePause(" + date[item].ID + ")' " + PuaseState + " value= '暂停' /></td>" +
                    "</tr>";
            }
            $("#matchInfo").append(htmlText);
        },
        error: function (date) {
            alert("getMatchPKInfoError" + date);
        }
    });
}

//改变状态，开启或关闭
function stateChange(Id) {
    $.ajax({
        url: "/Match/MatchChange?ID=" + Id,
        type: "POST",
        async: true,
        dataType: "json",
        success: function (date) {
            updateMatchPKInfo();
        },
        error: function (date) {
            window.log("stateChangeError" + date);
        }
    });
}

//暂停投票
function statePause(Id) {
    $.ajax({
        url: "/Match/StatePause?ID=" + Id,
        type: "POST",
        async: true,
        dataType: "json",
        success: function (date) {
            updateMatchPKInfo();
        },
        error: function (date) {
            window.log("stateChangeError" + date);
        }
    });
}