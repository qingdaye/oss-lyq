﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OSS.Domain;
using OSS.webUI.Apps;
using OSS.Service;
using OSS.Core;

namespace OSS.Controllers
{
    public class ResourceController : Controller
    {
        //
        // GET: /Class/
        public ActionResult Index()
        {
            IList<Course> listCourse = Container.Instance.Resolve<ICourseService>().GetAll();//LINQ

            return View(listCourse);
        }
        public JsonResult GetResourceInfo()
        {
            IList<Resource> listResource = Container.Instance.Resolve<IResourceService>().GetAll();

            return Json(listResource, JsonRequestBehavior.AllowGet);
        }
        public JsonResult addResource(Resource resource)
        {
            try
            {
                Container.Instance.Resolve<IResourceService>().Create(resource);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult updateResource(Resource resource)
        {
            try
            {
                Container.Instance.Resolve<IResourceService>().Update(resource);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteResource(Resource resource)
        {
            try
            {
                Container.Instance.Resolve<IResourceService>().Delete(resource);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}