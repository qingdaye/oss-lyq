﻿using OSS.Core;
using OSS.Domain;
using OSS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using NHibernate.Criterion;
using OSS.webUI.Apps;
using ProjectWenDangManage.Framework;
using MySql.Data.MySqlClient.Properties;

namespace OSS.webUI.Controllers
{
    public class UsersController : Controller
    {


        #region 登录
        [HttpGet] 
        public ViewResult Login()
        {
            Domain.User model = new User();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(string account,string pwd)
        //public ActionResult Login(User model)
        {
            pwd = AppHelper.EncodeMd5(pwd);//用MD5加密

            //访问数据库，根据用户名和密码获取用户信息
            IUserService userService = Container.Instance.Resolve<IUserService>();
            Domain.User loginedUser = userService.Login(account, pwd);
            if (loginedUser != null)//当用户名和密码正确时
            { 
                //获取当前登录对象的角色信息，待用
                AppHelper.LoginedUser = loginedUser;
                IList<User > listUser = Container.Instance.Resolve<IUserService>().GetAll();
                if (loginedUser.UserType.ToString() == "admin")
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return RedirectToAction("Index", "Users");
                }       
            }
            else
            {
                //如果登录不成功，则向用户提示错误信息
                ViewBag.ErrorMsg = "用户名或密码错误。";
                return View();
            }
        }
        #endregion

        [HttpGet]
        public JsonResult GetUsersInfo()
        {
            User user = new User();
            user.ID = AppHelper.LoginedUser.ID;
            user.UserImg = AppHelper.LoginedUser.UserImg;
            user.UserName = AppHelper.LoginedUser.UserName;
            user.UserPwd = AppHelper.LoginedUser.UserPwd;
            return Json(user,JsonRequestBehavior.AllowGet);         //保存登录信息，显示到母版页，跨控制器传数据
        }
        [HttpGet]
        public ActionResult Index()
        {

            IList<Course> CourseList = Container.Instance.Resolve<ICourseService>().GetAll();
            return View(CourseList); 
        }

      
        private void SetRoles()
        {
            IList<Role> roleList = Container.Instance.Resolve<IRoleService>().GetAll()
                .Where(m => m.IsActive == true)
                .ToList();
            ViewBag.RoleList = roleList;
        }
        [HttpGet]
        public ViewResult Create()
        {
            //实例化模型对象
            User user = new User();
            //准备视图需要的角色信息
            //SetRoles();
            return View(user);
        }
        /// <summary>
        /// 提交创建的用户
        /// 说明;获取表单值的方法如下
        /// 1、表示获取model对象，将model对象作为参数，可获取对象的所有值
        /// 2：表示获取指定控件的值，将控件名称(控件的name属性）作为参数，可获取该控件的值
        /// </summary>
        /// <param name="user"></param>
        /// <param name="hdSelectedIds"></param>
        /// <returns></returns>
        [HttpPost]
  
        public ActionResult Create(User user, string hdSelectedIds)
        {
            //确定模型验证是否通过
            user.UserPwd  = AppHelper.EncodeMd5(user.UserPwd);
            //user.Password = Container.Instance.Resolve<ISystemSettingService>().GetDefaultPassword();//获取系统默认的密码
            if(ModelState.IsValid)
            {
                //判断是否存在
                if(Container.Instance.Resolve<IUserService>().AccountCheck(0,user.UserAccount))
                {
                    ModelState.AddModelError("Account", "账号已经存在");//返回提示信息
                    //SetRoles();//提供角色试图显示
                    return View(user);
                }
               Container.Instance.Resolve<IUserService>().Create(user, hdSelectedIds.Replace(",,", ","));
                return RedirectToAction("login");
            }
            //SetRoles();
            return View(user);//停留在原始页面
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            //根据角色信息ID查询角色信息
            Role model = Container.Instance.Resolve<IRoleService>().Get(id);
            //以模型传值得方式将model传递到Create视图中显示
            return View("Create", model);
        }
        [HttpPost]
        public ActionResult Edit(Role model)
        {
            if (ModelState.IsValid)
            {
                Container.Instance.Resolve<IRoleService>().Update(model);

                return RedirectToAction("Index");
            }
            return View("Create", model);//停留在当前页面
        }
        /// <summary>
        /// 修改状态
        /// </summary>
        /// <param name="id">角色id</param>
        /// <returns></returns>
        public ActionResult SwitchStatus(int id)
        {
            Role model = Container.Instance.Resolve<IRoleService>().Get(id);

            model.IsActive = !model.IsActive;//操作一次就取反一次
            
            Container.Instance.Resolve<IRoleService>().Update(model);
            return RedirectToAction("Index");//返回到主页
        }
        [HttpGet]
        public ViewResult AssignRole(int id)
        {
            User user = Container.Instance.Resolve<IUserService>().Get(id);

            IList<Role> roleList = Container.Instance.Resolve<IRoleService>().GetAll().Where(m => m.IsActive == true).ToList();
            if(user.Roles!=null)
            {
                foreach(var item in roleList)
                {
                    if(user.Roles.Where(m=>m.ID==item.ID).Count()>0)
                    {
                        item.IsActive = true;
                    }
                }
            }
            ViewBag.RoleList = roleList;
            return View(user);
        }
        [HttpPost]
        public ActionResult AssignRole(int userId, string hdSelectedIds)
        { 
            Container.Instance.Resolve<IUserService>().AssignRole(userId,hdSelectedIds.Replace(",,",","));
            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult AddLearningRecords( int ResourceID)
        {
                //IList<ICriterion> query=new List<ICriterion>();
                //query.Add(Expression.And(Expression.Eq("User.ID", AppHelper.LoginedUser.ID), Expression.Eq("Resource.ID", ResourceID)));
                int countLearningRecords = Container.Instance.Resolve<ILearningRecordsService>().GetAll().Where(o=>o.Resources.ID==ResourceID&&o.User.ID==AppHelper.LoginedUser.ID).Count();
                if(countLearningRecords==0)
                {
                    LearningRecords learningRecords = new LearningRecords();
                    learningRecords.Resources = Container.Instance.Resolve<IResourceService>().Get(ResourceID);
                    learningRecords.User = AppHelper.LoginedUser;
                    Container.Instance.Resolve<ILearningRecordsService>().Create(learningRecords);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }

            //}
            //catch (Exception e)
            //{
            //    return Json(false, JsonRequestBehavior.AllowGet);
            //}
        }
        [HttpPost]
        public JsonResult UserLearnRecords()
        {
            IList<ICriterion> query = new List<ICriterion>();
            query.Add(Expression.Eq("User.ID", AppHelper.LoginedUser.ID));
            var lr = Container.Instance.Resolve<ILearningRecordsService>().GetAll(query);
            var resourse = (from temp in lr
                           select new
                           {
                               ResourcesName = temp.Resources.ResourcesName,
                               ResourcesImage = temp.Resources.ResourcesImage,
                               ResourcesInfo=temp.Resources.ResourcesInfo
                           }).ToList();

            return Json(resourse);
        }
    }
}