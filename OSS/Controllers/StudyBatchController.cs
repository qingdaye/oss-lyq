﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OSS.Domain;
using OSS.webUI.Apps;
using OSS.Service;
using OSS.Core;

namespace OSS.Controllers
{
    public class StudyBatchController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }
        public JsonResult GetStudyBatchInfo()
        {
            IList<StudyBatch> listStudyBatch = Container.Instance.Resolve<IStudyBatchService>().GetAll();
            //System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            //string batch=js.Serialize(listStudyBatch);
            return Json(listStudyBatch, JsonRequestBehavior.AllowGet);
        }
        public JsonResult addStudyBatch(StudyBatch studybatch)
        {
            try
            {
                Container.Instance.Resolve<IStudyBatchService>().Create(studybatch);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult updateStudyBatch(StudyBatch studybatch)
        {
            try
            {
                Container.Instance.Resolve<IStudyBatchService>().Update(studybatch);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteStudyBatch(StudyBatch studybatch)
        {
            try
            {
                Container.Instance.Resolve<IStudyBatchService>().Delete(studybatch);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}