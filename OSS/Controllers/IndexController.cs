﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OSS.Domain;
using OSS.Core;
using OSS.Service;

namespace OSS.webUI.Controllers
{
    public class IndexController : Controller
    {
        // GET: Index
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home()
        {
            IList<User> IUser = Container.Instance.Resolve<IUserService>().GetAll();
            return View(IUser);
        }
    }
}