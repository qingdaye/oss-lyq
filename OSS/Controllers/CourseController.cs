﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OSS.Domain;
using OSS.webUI.Apps;
using OSS.Service;
using OSS.Core;

namespace OSS.Controllers
{
    public class CourseController : Controller
    {
        //
        // GET: /Class/
        public ActionResult Index()
        {

            return View();
        }
        public JsonResult GetCourseInfo()
        {
            IList<Course > listCourse = Container.Instance.Resolve<ICourseService>().GetAll();

            return Json(listCourse, JsonRequestBehavior.AllowGet);
        }
        public JsonResult addCourse(Course course)
        {
            try
            {
                Container.Instance.Resolve<ICourseService>().Create(course);
            } catch (Exception e) {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult updateCourse(Course course)
        {
            try
            {
                Container.Instance.Resolve<ICourseService>().Update(course);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteCourse(Course course)
        {
            try
            {
                Container.Instance.Resolve<ICourseService>().Delete(course);
            }
            catch (Exception e)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
	}
}