﻿using OSS.Core;
using OSS.Service;
using OSS.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OSS.webUI.Apps;
using NHibernate.Criterion;

namespace OSS.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int pageIndex=1,string userName="")
        {
            IList<ICriterion> queryConditions = new List<ICriterion>();
            if (!string.IsNullOrEmpty(userName))
            {
                //模 糊匹配
                //queryConditions.Add(Expression.Like("Account",userName));
                queryConditions.Add(new LikeExpression("Account", userName));
            }

            int count = 0;//用于存放满足条件的记录总
            //3设置排序表达式集合(必须的修改)
            IList<Order> listOrder = new List<Order>() {
                new Order("ID", true)
            };//设置一


            //IList<User> list = Container.Instance.Resolve<IUserService>().GetAll();
            IList<User> list = Container.Instance.Resolve<IUserService>()
               .GetPaged(queryConditions, listOrder, pageIndex, PagerHelper.PageSize, out count);

            PageList<User> pageList =
           list.ToPageList<User>(pageIndex, PagerHelper.PageSize, count);
            return View(pageList); 

        }
        /// <summary>
        /// 查看明细
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
 
        [HttpGet]
        public ActionResult Edit(int id)//当调用edit页面时将传入一个id,这个id代表要修改的哪一行数据
        {
            User user = Container.Instance.Resolve<IUserService>().Get(id);//根据id获取user实体
            return View(user);//将这个user实体显示在edit页面上对应的控件
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(User model)//当用户点击“保存”按钮时要执行的方法
        {
            if (ModelState.IsValid)//如果实体的数据合法
            {
                //从数据库中通过ID获取完整的用户信息（修改之前的数据）
                User newUser = Container.Instance.Resolve<IUserService>().Get(model.ID);
                //用视图中新修改的值替换掉查询回来的值
                newUser.UserName = model.UserName;
                newUser.UserAccount = model.UserAccount;
                newUser.UserPwd = model.UserPwd;
                newUser.UserImg = model.UserImg;

                Container.Instance.Resolve<IUserService>().Update(newUser);//更新实体
                return RedirectToAction("Details");//跳转到列表页面
            }
            return View(model);//停留在原页面
        }
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
     
        public ActionResult Delete(int id)
        {
           Container.Instance.Resolve<IUserService>().Delete(id);
           return RedirectToAction("Details");//跳转到列表视图
        }
       
        public ViewResult UnAuthorized()
        {
            return View();
        }
        /// <summary>
        /// 更改资料
        /// </summary>
        /// <returns></returns>
        public ActionResult Modify()
        {
            return View();
        }

        public JsonResult updateUser(User user)
        {
            User newUser = Container.Instance.Resolve<IUserService>().Get(user.ID);
             try
            {
                newUser.UserPwd = AppHelper.EncodeMd5(user.UserPwd);
                newUser.UserName = user.UserName;
                newUser.UserImg = user.UserImg;
                Container.Instance.Resolve<IUserService>().Update(newUser);
                AppHelper.LoginedUser = newUser;
                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }

    }
}