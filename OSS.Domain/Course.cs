﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using System.ComponentModel.DataAnnotations;

namespace OSS.Domain
{
    [ActiveRecord]
    public class Course:EntityBase
    {
        [Display(Name = "课程名称")]
        [Property]
        public string CourseName { get; set; }

        [Display(Name = "课程信息")]
        [Property]
        public string CourseInfo { get; set; }

        [Display(Name ="课程代码")]
        [Property]
        public int CourseCode { get; set; }

        [HasMany(typeof(Resource),
            Table = "resource",
            ColumnKey = "CourseID",
            Cascade = ManyRelationCascadeEnum.Delete,
            Lazy = false,
            Inverse = false)]
        public virtual IList<Resource> resource { get; set; }


        //[HasAndBelongsToMany(typeof(Resource),
        //    Table = "Course_Resourse",
        //    ColumnKey = "CourseID",
        //    ColumnRef = "ResourceID",
        //    Cascade = ManyRelationCascadeEnum.None,
        //    Inverse = false,
        //    Lazy = false)]
        //public IList<Resource> Resources { get; set; }

    }
}
