﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using System.ComponentModel.DataAnnotations;

namespace OSS.Domain
{
    [ActiveRecord]
    public class LearningRecords : EntityBase
    {
        [Display(Name = "用户")]
        [BelongsTo(Type = typeof(User), Column = "UserID")]
        public User User { get; set; }   //用户ID

        [Display(Name = "资源")]
        [BelongsTo(Type = typeof(Resource), Column = "ResourceID")]
        public Resource Resources { get; set; }   //课程ID


        [Display(Name ="加入时间")]
        [Property]
        public DateTime JoinTime { get; set; } //加入时间

        [Display(Name ="结束时间")]
        [Property]
        public DateTime EndTime { get; set; }  //结束时间

    }
}
