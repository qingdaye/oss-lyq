﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using System.ComponentModel.DataAnnotations;

namespace OSS.Domain
{
    [ActiveRecord("sysuser")]
    public class User:EntityBase
    {
        [Display(Name = "账号")]
        [Property(NotNull =true,Length =16)]
        public string UserAccount { get; set; }

        [Display(Name = "用户名")]
        [Property(NotNull =false,Length =20)]
        public string UserName { get; set; }

        [Display(Name = "密码")]
        [Property(NotNull =true,Length =16)]
        public string UserPwd { get; set; }

        [Display(Name = "头像")]
        [Property(NotNull = true, Length = 200)]
        public string UserImg { get; set; }

        [Property]
        [Display(AutoGenerateField = false)]
        public UserType UserType { get; set; }

        [Property]
        [Display(AutoGenerateField = false)]
        public bool IsActive { get; set; }

        /// <summary>
        /// 当前用户拥有的角色
        /// </summary>
        [HasAndBelongsToMany(typeof(Role),
            Table = "user_role",
            ColumnKey = "UserID",
            ColumnRef = "RoleID",
            Cascade = ManyRelationCascadeEnum.None,
            Inverse = false,
            Lazy = false)]
        public virtual IList<Role> Roles { get; set; }

        [HasMany(typeof(LearningRecords), Table = "learningrecords",
            ColumnKey = "UserID",
            Cascade = ManyRelationCascadeEnum.SaveUpdate,
            Lazy = true, Inverse = false)]
        [Display(Name = "记录")]
        public IList<LearningRecords> learningRecords { get; set; }
        //[HasAndBelongsToMany(typeof(StudyBatch),
        //    Table = "user_studybatch",
        //    ColumnKey = "SysUserID",
        //    ColumnRef = "StudyBatchID",
        //    Cascade = ManyRelationCascadeEnum.None,
        //    Inverse = false,
        //    Lazy = false)]
        //public IList<StudyBatch> StudyBatchs { get; set; }
    }
}
