﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.ActiveRecord;
using System.ComponentModel.DataAnnotations;

namespace OSS.Domain
{
    public class EntityBase
    {
        [PrimaryKey(PrimaryKeyType.Identity)]//主键的类型为自增型
        [Display(AutoGenerateField = false)]//在生成视图时不生成本属性
        public virtual int ID { get; set; }//主键

        //[Property("Version")]//数据库中与Version属性对应的字段
        //[Display(AutoGenerateField = false)]//在生成视图时不生成本属性
        //public virtual int Version { get; set; }//数据版本，实现同步数据处理
    }
}
