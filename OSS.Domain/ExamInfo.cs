﻿using Castle.ActiveRecord;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSS.Domain
{
    [ActiveRecord]
    public class ExamInfo:EntityBase
    {

        [Display(Name ="用户名")]
        [BelongsTo(Type=typeof(User),Column = "SysUserID")]
        public User Users { get; set; }

        [Display(Name ="学习批次")]
        [BelongsTo(Type =typeof(StudyBatch),Column = "StudyBatchID")]
        public StudyBatch StudyBatch { get; set; }

        [Display(Name = "得分")]
        [Property]
        public string Score { get; set; }       //得分

    }
}
