﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using System.ComponentModel.DataAnnotations;

namespace OSS.Domain
{
    [ActiveRecord]
    public class Resource : EntityBase
    {
        [Display(Name = "资源名称")]
        [Property]
        public string ResourcesName { get; set; }

        [Display(Name = "资源路径")]
        [Property]
        public string ResourcesUrl { get; set; }

        [Display(Name = "资源预览图")]
        [Property]
        public string ResourcesImage { get; set; }

        [Display(Name = "资源描述")]
        [Property]
        public string ResourcesInfo { get; set; }

        [Display(Name = "是否激活")]
        [Property]
        public string IsActive { get; set; }


        
        //[HasAndBelongsToMany(typeof(StudyBatch),
        //    Table = "resource_studybatch",
        //    ColumnKey = "ResourceID",
        //    ColumnRef = "StudyBatchID",
        //    Cascade = ManyRelationCascadeEnum.None,
        //    Inverse = false,
        //    Lazy = false)]
        //public IList<StudyBatch> StudyBatchs { get; set; }

    }
}
