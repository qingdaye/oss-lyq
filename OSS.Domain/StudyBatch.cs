﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.ActiveRecord;
using System.ComponentModel.DataAnnotations;

namespace OSS.Domain
{
    [ActiveRecord]
    public class StudyBatch : EntityBase
    {
        [Display(Name = "批次名称")]
        [Property]
        public string StudyBatchName { get; set; }

        [Display(Name = "开始时间")]
        [Property]
        public DateTime StartTime { get; set; }

        [Display(Name = "结束时间")]
        [Property]
        public DateTime EndTime { get; set; }

        [Display(Name = "是否可以参加考试")]
        [Property]
        public bool CanExam { get; set; }

        //[HasAndBelongsToMany(typeof(User),
        //    Table = "user_studybatch",
        //    ColumnKey = "StudyBatchID",
        //    ColumnRef = "SysUserID",
        //    Inverse = false,
        //    Lazy = false )]
        //public IList<User> Users { get; set; }

        //[HasAndBelongsToMany(typeof(Resource),
        //    Table = "resource_studybatch",
        //    ColumnKey = "StudyBatchID",
        //    ColumnRef = "ResourceID",
        //    Inverse = false,
        //    Lazy = false )]
        //public IList<Resource> ResourceID { get; set; }
    }
}
