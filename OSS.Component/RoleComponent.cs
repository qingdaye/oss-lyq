﻿using OSS.Domain;
using OSS.Manager;
using OSS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSS.Component
{
    public class RoleComponent:BaseComponent<Role, RoleManager>, IRoleService
    {
        /// <summary>
        /// 配置角色对应的权限
        /// </summary>
        /// <param name="roleID">要分配权限的角色ID</param>
        /// <param name="systemfunction">用,隔开的功能模块ID，如1,2,3...</param>
        public void AssignAuthorize(int roleID)
        {
            //获取要修改权限的角色
            Role role = manager.Get(roleID);

          
            //更新角色，同时更新角色所对应的功能
            manager.Update(role);

        }
    }
}
