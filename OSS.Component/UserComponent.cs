﻿using NHibernate.Criterion;
using Castle.Services.Transaction;
using Castle.ActiveRecord.Queries;
using OSS.Domain;
using OSS.Manager;
using OSS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSS.Component
{
    public class UserComponent:BaseComponent<User,UserManager>,IUserService
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account">操作员输入的用户名</param>
        /// <param name="password">操作员输入的密码</param>
        /// <returns>当用户名和密码成功匹配时返回匹配的用户信息，否则返回null</returns>
        public User Login(string account, string password)
        {
            //组织查询条件(标准条件查询)
            IList<NHibernate.Criterion.ICriterion> criterionList = new List<ICriterion>();
            //向条件集合添加查询条件，每个条件之间默认为And关系，从数据库中查询同时满足3个条件的用户信息
            criterionList.Add(Expression.Eq("UserAccount", account));
            criterionList.Add(Expression.Eq("UserPwd", password));
            criterionList.Add(Expression.Eq("IsActive", true));

            //调用数据访问层的方法执行操作       
            User user = manager.Get(criterionList);
            return user;
        }

        public void Create(User user, string roleId)
        {
            if (!string.IsNullOrEmpty(roleId))
            {
                if (user.Roles == null)
                {
                    user.Roles = new List<Role>();
                }
                string[] ids = roleId.Split(new char[] { ',', '_', '|' });
                foreach (string id in ids)
                {
                    if (string.IsNullOrEmpty(id))
                    {
                        continue;
                    }
                    user.Roles.Add(new RoleComponent().Get(int.Parse(id)));
                }
            }
            manager.Create(user);
        }
        /// <summary>
        /// 修改状态
        /// </summary>
        /// <param name="id">角色id</param>
        /// <returns></returns>
        public void SwitchStatus(int id)
        {
            User user = this.Get(id);
            user.IsActive = !user.IsActive;
            Update(user);
        }

        /// <summary>
        /// 通过id删除
        /// </summary>
        /// <param name="id"></param>
        public new void Delete(int id)
        {
            manager.Delete(id);
        }
        /// <summary>
        /// 通过对象删除
        /// </summary>
        /// <param name="user"></param>
        public void Delete(User user)
        {
            manager.Delete(user);
        }
        /// <summary>
        /// 账号重复性检测
        /// </summary>
        /// <param name="id"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public bool AccountCheck(int? id, string account)
        {
            IList<ICriterion> clist = new List<ICriterion>();
            clist.Add(Expression.Eq("ID", id.Value));
            if (id.HasValue && id.Value > 0)
            {
                ICriterion ct = Expression.Eq("ID", id.Value);
                clist.Add(Expression.Not(ct));
            }


            return this.Exists(clist);
        }
        public void AssignRole(int userId, string roleId)
        {
            User user = manager.Get(userId);
            if (user.Roles == null)
            {
                user.Roles = new List<Role>();
            }
            //清空所有的角色
            user.Roles.Clear();
            if (!string.IsNullOrEmpty(roleId))
            {
                string[] ids = roleId.Split(new char[] { ',', '_', '|' });
                foreach (string item in ids)
                {
                    if (string.IsNullOrEmpty(item))
                    {
                        continue;
                    }
                    //增加角色到集合中
                    user.Roles.Add(new RoleManager().Get(int.Parse(item)));
                }
            }
        }

    }
}
