﻿using OSS.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSS.Service
{
    public interface IRoleService :IBaseService<Role>
    {
        /// <summary>
        /// 分配角色所对应的功能
        /// </summary>
        /// <param name="roleID">要分配权限的角色</param>
        /// <param name="systemfunctions">用逗号(,)隔开的功能模块ID，如1,2,3...</param>
        void AssignAuthorize(int RoleID);
    }
}
