﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OSS.Domain;

namespace OSS.Service
{
    public interface ISystemSettingService
    {
        /// <summary>
        /// 初始化数据库
        /// </summary>
        void InitDataBase();

        /// <summary>
        /// 获取系统默认密码
        /// </summary>
        /// <returns>用MD5加密后的密码</returns>
        string GetDefaultPassword();

    }
}
